<?php

declare(strict_types=1);

use Framework\Http\EmitResponseToSapi;

use Framework\Http\Message\ResponseFactory;
use Framework\Http\Message\ServerRequestFactory;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use function DetectLang\detectLang;

http_response_code(500);

/** @psalm-suppress MissingFile */
require __DIR__ . '/../vendor/autoload.php';

### Page example
final class Home
{
    private readonly ResponseFactoryInterface $responseFactory;

    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $name = $request->getQueryParams()['name'] ?? 'Guest';

        if (!is_string($name)) {
            return $this->responseFactory->createResponse(400);
        }

        $lang = detectLang($request, 'en');

        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'text/plain; charset=utf-8');

        $response->getBody()->write('Hello, ' . $name . '! Your lang is ' . $lang);

        return $response;
    }
}

### Grabbing
$request = ServerRequestFactory::createServerRequestFromGlobals();

### Preprocessing
if (str_starts_with($request->getHeaderLine('Content-Type'), 'application/x-www-form-urlencoded')) {
    parse_str((string) $request->getBody(), $data);
    $request = $request->withParsedBody($data);
}

### Running
$home = new Home(new ResponseFactory());
$response = $home($request);

### Postprocessing
$response = $response->withHeader('X-Frame-Options', 'DENY');

### Sending
$emitter = new EmitResponseToSapi();
$emitter->emit($response);
